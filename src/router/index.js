import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    component: Home,
    meta: [],
    children: [
      {
        path: '/',
        name: 'Index',
        component: () => import ('../components/Index.vue'),
        meta: [
          {
            title: '首页'
          }
        ]
      }, {
        path: '/i',
        name: 'Me',
        component: () => import ('../components/account/Me.vue'),
        meta: [
          {
            title: '用户信息'
          }
        ]
      }, {
        path: '/task',
        name: 'Task',
        component: () => import ('../components/account/Task.vue'),
        meta: [
          {
            title: '我的任务'
          }
        ]
      }, {
        path: '/list1',
        name: 'list1',
        component: () => import ('../components/data/list1.vue'),
        meta: [
          {
            title: '查询+表格+分页'
          }
        ]
      }, {
        path: '/list2',
        name: 'list2',
        component: () => import ('../components/data/list2.vue'),
        meta: [
          {
            title: '页签+查询+表格+分页'
          }
        ]
      }, {
        path: '/form1',
        name: 'form1',
        component: () => import ('../components/form/form1.vue'),
        meta: [
          {
            title: '基本表单'
          }
        ]
      }
    ]
  }, {
    path: '/login',
    name: 'Login',
    component: () => import ('../views/Login.vue')
  }
]

const router = new VueRouter({
  routes, mode: 'history' //去掉url中的#
})

export default router
